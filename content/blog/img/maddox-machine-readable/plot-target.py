import pandas
import matplotlib.pyplot as plt

data = pandas.read_csv("sensitivity.csv")

data.plot(
    y="target",
    kind="bar"
)

plt.savefig("target.png")