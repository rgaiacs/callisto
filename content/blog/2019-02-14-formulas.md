---
title: Formulas
date: 2019-02-14
---

Spreadsheet softwares allow you to use formula to do some calculations.
For example,
you can use `=SUM(B6:B21)`
to get the sum of the values of the cells highlighted on the screenshot below.

![Screenshot of LibreOffice Calc with some cells selected.](../img/maddox-non-machine-readable-formulas.png)

Callisto supports Python and R to do calculations
and make all the data available out of the box to you.
If you decide use R,
you can use

```
sum(sensitivity$target)
```

to calculate the same result.